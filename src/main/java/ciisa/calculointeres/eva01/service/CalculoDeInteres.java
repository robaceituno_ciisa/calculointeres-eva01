/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.calculointeres.eva01.service;

/**
 *
 * @author mono
 */
public class CalculoDeInteres {
    private Double monto;
    private Double tasa;
    private Double agnos;

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Double getTasa() {
        return tasa;
    }

    public void setTasa(Double tasa) {
        this.tasa = tasa;
    }

    public Double getAgnos() {
        return agnos;
    }

    public void setAgnos(Double agnos) {
        this.agnos = agnos;
    }

    
    
    public Long getMontoFinalCredito() {
        System.out.println("*************** monto : " + monto);
        System.out.println("*************** tasa : " + tasa);
        System.out.println("*************** años : " + agnos);
        
        Double resultado = monto + ((monto * (tasa/100)) * agnos);
        System.out.println("*************** Resultado : " + resultado);
        return Math.round(resultado);
    }
    
}
