<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css"/>
        <title>Calculadora de Interes</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">

            <div class="mx-auto order-0">
                <a class="navbar-brand mx-auto" href="#"><h1>Resultado : Calculo de Interes Simple</h1></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <br>
        <div class="container-fluid h-100">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
                    <form name="form" action="controller" method="POST" >
                        <%
                            String monto = (String) request.getAttribute("monto");
                            String tasa = (String) request.getAttribute("tasa");
                            String agnos = (String) request.getAttribute("agnos");
                            String montototal = (String) request.getAttribute("totalcredito");
                            String interes = (String) request.getAttribute("interes");
                        %>
                        <br>
                        <div class="form-group">
                            <label><h4>Monto  : <%=monto%></h4></label>
                        </div>
                        <div class="form-group">
                            <label><h4>Tasa : <%=tasa%></h4></label>
                        </div>
                        <div class="form-group">
                            <label><h4>Años : <%=agnos%></h4></label>
                        </div>
                        <div class="form-group">
                            <label><h4>Monto total crédito : <%=montototal%></h4></label>
                        </div>
                        <div class="form-group">
                            <label><h4>El interes simple es : <%=interes%></h4></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>s
        <footer class="fixed-bottom">
            <div class="container-fluid text-center bg-dark text-white">
                <span>Taller Aplicaciones Empresariales - UNI01 - CIISA 2020 ::: Robinson Aceituno</span>
            </div>
        </footer>
    </body>
</html>
